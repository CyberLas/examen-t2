﻿using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Repositories;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalidadT2NUit.ControllerTest
{
    class HomeControllerTest
    {
        [Test]
        public void MostrarLibrosConAutor() 
        {
            var app = new Mock<IHomeRepository>();
            app.Setup(o => o.GetBooks()).Returns(new List<Libro>());

            var controller = new HomeController(app.Object);
            var index = controller.Index();
            Assert.IsInstanceOf<ViewResult>(index);
        }

    }
}
