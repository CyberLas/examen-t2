﻿using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Repositories;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalidadT2NUit.ControllerTest
{
    class AuthControllerTest
    {
        [Test]
        public void DebeRetornarVista_IActionResult() 
        {
            var app = new Mock<IAuthRepository>();
            var controller = new AuthController(null,null);
            var index = controller.Login();
            Assert.IsInstanceOf<IActionResult>(index); 
        }
        
        [Test]
        public void LoginRedireccionaraMismaPagina()
        {
            var app = new Mock<IAuthRepository>();
            app.Setup(o => o.FindUser("nofunciona", "nofunciona"))
                .Returns(new Usuario { Username = "nofunciona", Password = "nofunciona" });

            var controller = new AuthController(app.Object, null);
            var result = controller.Login("nofunciona", "nofunciona");

            Assert.IsInstanceOf<RedirectToActionResult>(result);
        }

        
        [Test]
        public void DebeRetornarVista_IActionSalida()
        {
            var app = new Mock<IAuthRepository>();
            var controller = new AuthController(app.Object,null);
            var index = controller.Logout();
            Assert.IsInstanceOf<IActionResult>(index); 
        }

    }
}
