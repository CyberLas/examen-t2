﻿using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Repositories;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalidadT2NUit.ControllerTest
{
    class BibliotecaControllerTest
    {
        [Test]
        public void DebeRetornarVista_IActionResult() 
        {
            var app = new Mock<IBibliotecaRepository>();
            app.Setup(o => o.GetBook()).Returns(new List<Biblioteca>());

            var controller = new BibliotecaController(app.Object);
            var index = controller.Index();
            Assert.IsInstanceOf<ViewResult>(index);
        }

    }
}
