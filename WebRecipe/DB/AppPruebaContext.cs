﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Threading.Tasks;
using WebRecipe.DB.Configurations;
using WebRecipe.Models;

namespace WebRecipe.DB
{
    public class AppPruebaContext : DbContext
    {
        public DbSet <Dish>  Dishes                     { get; set; }
        public DbSet <Chef>  Chefs                      { get; set; }
        public DbSet<Ingredient> Ingredients            { get; set; }
        public DbSet<Preparation> Preparations          { get; set; }
        public DbSet<Characteristic> Characteristics    { get; set; }
        public DbSet<Follow> Follows                    { get; set; }
        public DbSet<Comment> Comments                 { get; set; }
        public DbSet<Favoritos> FavoritosS             { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=web-recipe.mssql.somee.com;Initial Catalog=web-recipe;User ID=cyberlas_SQLLogin_2;Password=s9hi1cfc32");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            // coddigo tabla tabla
            modelBuilder.ApplyConfiguration(new DishConfiguration());
            modelBuilder.ApplyConfiguration(new ChefConfiguration());
            modelBuilder.ApplyConfiguration(new IngredientConfiguration());
            modelBuilder.ApplyConfiguration(new PreparationConfiguration());
            modelBuilder.ApplyConfiguration(new CharacteristicConfiguration());
            modelBuilder.ApplyConfiguration(new FollowConfiguration());
            modelBuilder.ApplyConfiguration(new CommentConfiguration());
            modelBuilder.ApplyConfiguration(new FavoritosConfiguration());
        }
    }
}
