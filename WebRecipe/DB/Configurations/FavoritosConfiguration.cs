﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebRecipe.Models;

namespace WebRecipe.DB.Configurations
{
    public class FavoritosConfiguration : IEntityTypeConfiguration<Favoritos>
    {
        public void Configure(EntityTypeBuilder<Favoritos> builder)
        {
            builder.ToTable("Favoritos");
            builder.HasKey(o => o.IdFavorito);
        }
    }
}
