﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebRecipe.Models
{
    public class Comment
    {
        public int IdComments       { get; set; }
        public int IdChef           { get; set; }
        public int IdReceta         { get; set; }
        public String Comentario    { get; set; }
        public int SubComentario   { get; set; }
        public DateTime Fecha       { get; set; }
    }
}
