﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebRecipe.Models
{
    public class Follow
    {
        public int IdFollow     { get; set; }
        public int IdCheff      { get; set; }
        public int IdSeguidor   { get; set; }
        public DateTime Fecha     { get; set; }

    }
}
