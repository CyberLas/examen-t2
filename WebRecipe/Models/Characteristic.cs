﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebRecipe.Models
{
    public class Characteristic
    {
        public int IdCharacteristic { get; set; }
        public int IdDish { get; set; }
        [Required]
        public int Kalorias { get; set; }
        [Required]
        public int Carbohidratos { get; set; }
        [Required]
        public int Proteinas { get; set; }
        [Required]
        public int Grasas { get; set; }
    }
}
