﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebRecipe.Models
{
    public class Preparation
    {
        public int IdPreparation { get; set; }
        [Required]
        public int IdDish { get; set; }
        [Required]
        public String Descripcion { get; set; }
        public String Nota { get; set; }
        [Required]
        public String Imagen { get; set; }
    }
}
