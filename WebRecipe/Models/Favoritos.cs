﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebRecipe.Models
{
    public class Favoritos
    {
        public int IdFavorito { get; set; }
        public int IdChef { get; set; }
        public int IdDish { get; set; }
    }
}
