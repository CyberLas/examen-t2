﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebRecipe.Models
{
    public class Dish
    {
        public int IdDish { get; set; }
        [Required]
        public String Plato { get; set; }
        [Required]
        public int IdChef { get; set; }
        [Required]
        public String Video { get; set; }
        [Required]
        public String Imagen { get; set; }
        [Required]
        public String Descripcion { get; set; }
    }
}
