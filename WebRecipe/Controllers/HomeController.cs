﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using WebRecipe.DB;
using WebRecipe.Extensions;
using WebRecipe.Models;

namespace WebRecipe.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            var context = new AppPruebaContext();
            ViewBag.Dishes = context.Dishes.ToList().OrderBy(o => Guid.NewGuid()).Take(6).ToList();
            return View();
        }


        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(string email, string password)
        {
            var context = new AppPruebaContext();
            var user = context.Chefs.FirstOrDefault(o => o.Correo == email && o.Password == password);

            if (user == null)
                return View();

            //Guarda en memoria cache
            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, user.Correo)
            };

            HttpContext.Session.Set("SessionLoggedUser", user);

            var userIdentity = new ClaimsIdentity(claims, "login");
            var principal = new ClaimsPrincipal(userIdentity);

            HttpContext.SignInAsync(principal);

            return RedirectToAction("Index", "Dashboard");
        }


        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
