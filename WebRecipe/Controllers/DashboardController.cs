﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebRecipe.DB;
using WebRecipe.Extensions;
using WebRecipe.Models;


namespace WebRecipe.Controllers
{
    [Authorize]
    public class DashboardController : Controller
    {

        [HttpGet]
        public IActionResult Index()
        {
            var context = new AppPruebaContext();
            ViewBag.Dishes = context.Dishes.ToList().Take(6).ToList();
            return View();
        }

        [HttpGet]
        public IActionResult Follow()
        {
            var context = new AppPruebaContext();
            var usserLogged = HttpContext.Session.Get<Chef>("SessionLoggedUser");
            ViewBag.Chefs = context.Chefs.ToList();
            ViewBag.Follow = context.Follows.Where(o => o.IdCheff == usserLogged.IdChef).ToList();
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            var context = new AppPruebaContext();
            ViewBag.Chefs = context.Chefs.ToList();
            return View();
        }

        [HttpGet]
        public IActionResult Comment()
        {
            var context = new AppPruebaContext();
            var usserLogged = HttpContext.Session.Get<Chef>("SessionLoggedUser");
            ViewBag.User = usserLogged;
            ViewBag.Cheff = context.Chefs.ToList();
            ViewBag.Comment = context.Comments.ToList();
            return View();
        }

        [HttpPost]
        public IActionResult Create(Dish model)
        {
            var context = new AppPruebaContext();
            var usserLogged = HttpContext.Session.Get<Chef>("SessionLoggedUser");

            if (ModelState.IsValid)
            {
                var receta = new Dish();
                receta.Plato = model.Plato;
                receta.Video = model.Video;
                receta.Imagen = model.Imagen;
                receta.Descripcion = model.Descripcion;
                receta.IdChef = usserLogged.IdChef;
                context.Dishes.Add(receta);
                context.SaveChanges();

                return Redirect("/Dashboard/Index");

            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Profile()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Recipe(int id)
        {
            var context = new AppPruebaContext();
            ViewBag.Random = context.Dishes.ToList();
            ViewBag.Dishes = context.Dishes.Where(o => o.IdDish == id).ToList();
            ViewBag.Ingredient = context.Ingredients.Where(o => o.IdDish == id).ToList();
            ViewBag.Characteristic = context.Characteristics.Where(o => o.IdDish == id).ToList();
            ViewBag.Preparation = context.Preparations.Where(o => o.IdDish == id).ToList();
            ViewBag.Chef = context.Chefs.ToList();
            return View();
        }


        [HttpGet]
        public IActionResult Buscar(String name)
        {
            var context = new AppPruebaContext();
            ViewBag.Dishes = context.Dishes.Where(o => o.Plato.Contains(name));
            ViewBag.Chef = context.Chefs.ToList();
            return View();
        }

        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.SignOutAsync();
            return RedirectToAction("Login", "Home");
        }

        [HttpGet]
        public IActionResult AñadirFavorito(int id)
        {
            var context = new AppPruebaContext();
            var usserLogged = HttpContext.Session.Get<Chef>("SessionLoggedUser");

            var usuarioid= context.Chefs.Where(o => o.IdChef == usserLogged.IdChef).FirstOrDefault();
            var favorito = new Favoritos();
            favorito.IdChef = usuarioid.IdChef;
            favorito.IdDish = id;

            context.FavoritosS.Add(favorito);
            context.SaveChanges();
            return Redirect("/Dashboard/Index");
        }
        [HttpGet]
        public IActionResult QuitarFavorito(int id)
        {
            var context = new AppPruebaContext();
            var usserLogged = HttpContext.Session.Get<Chef>("SessionLoggedUser");

            var usuarioid = context.Chefs.Where(o => o.IdChef == usserLogged.IdChef).FirstOrDefault();

            var favorito = context.FavoritosS.Where(o => o.IdChef == usserLogged.IdChef)
                .Where(o => o.IdDish == id).FirstOrDefault();

            context.FavoritosS.Remove(favorito);
            context.SaveChanges();
            return Redirect("/Dashboard/Index");
        }
        
    }
}
