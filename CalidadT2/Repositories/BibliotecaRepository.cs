﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CalidadT2.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CalidadT2.Repositories
{
    public interface IBibliotecaRepository
    {
        public List<Biblioteca> GetBook();

        public List<Usuario> GetUsuario();
        List<Biblioteca> GetBooks(int a, string b, string c, int d);
        void SaveChanges();
    }

    public class BibliotecaRepository : IBibliotecaRepository
    {
        private readonly AppBibliotecaContext context;
        public BibliotecaRepository(AppBibliotecaContext context) 
        {
            this.context = context;
        }

        public List<Biblioteca> GetBook()
        {
            return context.Bibliotecas
                .Include(o => o.Libro.Autor)
                .Include(o => o.Usuario)
                .Where(o => o.UsuarioId == 2)
                .ToList();
        }

        public List<Usuario> GetUsuario()
        {
            return context.Usuarios.Where(o => o.Username == "admin").ToList();
        }

        public void SaveChanges()
        {
            var a = context.SaveChanges();
        }

        List<Biblioteca> IBibliotecaRepository.GetBooks(int a, string b, string c, int d)
        {
            return context.Bibliotecas
                .Where(o => o.UsuarioId == a)
                .Where(o => o.Id == d).ToList();
        }
    }
}
