﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CalidadT2.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CalidadT2.Repositories
{
    public interface ILibroRepository
    {
        public List<Libro> GetLibroInclude(int id);
        public Usuario FindUser(string username, string password);
        void SaveChanges();
        public int GetComment(Comentario comentario);
        public List<Libro> GetBook(Comentario comentario);

        public List<Usuario> GetUsuario();
    }

    public class LibroRepository : ILibroRepository
    {
        private readonly AppBibliotecaContext context;
        public LibroRepository(AppBibliotecaContext context) 
        {
            this.context = context;
        }

        public Usuario FindUser(string username, string password)
        {
            return context.Usuarios.Where(o => o.Username == username && o.Password == password).FirstOrDefault();
        }

        public List<Libro> GetLibroInclude(int id)
        {
            return context.Libros
               .Include("Autor")
               .Include("Comentarios.Usuario")
               .Where(o => o.Id == id).ToList();
        }

        public void SaveChanges()
        {
           var a = context.SaveChanges();
        }

        public int GetComment(Comentario comentario)
        {
            var a = context.Comentarios.Add(comentario);
            return 0;
        }

        public List<Libro> GetBook(Comentario comentario)
        {
            return context.Libros.Where(o => o.Id == comentario.LibroId).ToList();
        }

        public List<Usuario> GetUsuario()
        {
            return context.Usuarios.Where(o => o.Username == "admin").ToList();
        }
    }

}
