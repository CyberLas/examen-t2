﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CalidadT2.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CalidadT2.Repositories
{
    public interface IHomeRepository
    {
        public List<Libro> GetBooks();
    }

    public class HomeRepository : IHomeRepository
    {
        private readonly AppBibliotecaContext context;
        public HomeRepository(AppBibliotecaContext context) 
        {
            this.context = context;
        }

        public List<Libro> GetBooks()
        {
            return context.Libros.Include(o => o.Autor).ToList();
        }
    }

}
