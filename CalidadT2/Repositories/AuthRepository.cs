﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CalidadT2.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CalidadT2.Repositories
{
    public interface IAuthRepository
    {
        public Usuario FindUser(string username, string password);
    }

    public class AuthRepository : IAuthRepository
    {
        private readonly AppBibliotecaContext context;
        public AuthRepository(AppBibliotecaContext context)
        {
            this.context = context;
        }

        public Usuario FindUser(string username, string password)
        {
            return context.Usuarios.Where(o => o.Username == username && o.Password == password).FirstOrDefault();
        }

    }
}
