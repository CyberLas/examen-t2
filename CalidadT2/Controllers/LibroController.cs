﻿using System;
using System.Linq;
using CalidadT2.Models;
using CalidadT2.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CalidadT2.Controllers
{
    public class LibroController : Controller
    {
        private readonly ILibroRepository app;

        public LibroController(ILibroRepository app)
        {
            this.app = app;
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            var model = app.GetLibroInclude(id)[0];
            return View(model);
        }
        
        [HttpPost]
        public IActionResult AddComentario(Comentario comentario)
        {
            var user = app.FindUser("admin", "admin" );
            comentario.UsuarioId = user.Id;
            comentario.Fecha = DateTime.Now;
            app.GetComment(comentario);

            var libro = app.GetBook(comentario)[0];
            libro.Puntaje = (libro.Puntaje + comentario.Puntaje) / 2;

            app.SaveChanges();

            return RedirectToAction("Details", new { id = comentario.LibroId });
        }
        
        private Usuario LoggedUser()
        {
            var user = app.GetUsuario()[0];
            return user;
        }
    }
}
