﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CalidadT2.Models;
using CalidadT2.Repositories;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace CalidadT2.Controllers
{
    public class AuthController : Controller
    {
        private readonly IConfiguration configuration;
        private readonly IAuthRepository app;
        
        public AuthController(IAuthRepository app,IConfiguration configuration)
        {
            this.configuration = configuration;
            this.app = app;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View(0);
        }
        
        [HttpPost]
        public IActionResult Login(string username, string password)
        {
            var usuario = app.FindUser(username, password);
            if (usuario != null)
            {
                var claims = new List<Claim> {
                    new Claim(ClaimTypes.Name, username)
                };

                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);


                return RedirectToAction("Index", "Home");
            }
            
            ViewBag.Validation = "Usuario y/o contraseña incorrecta";
            return View();
        }

        public ActionResult Logout()
        {
            return RedirectToAction("Login");
        }
    }
}
